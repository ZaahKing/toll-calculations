﻿using System;

namespace TollCalculations
{
    /// <summary>
    /// VehicleTollCalculator.
    /// </summary>
    public abstract class VehicleTollCalculator : ITollCalculator
    {
        /// <inheritdoc/>
        public abstract decimal CalculateToll();

        /// <inheritdoc/>
        public decimal PeakTimePremium(DateTime timeOfToll, bool inbound)
        {
            if ((timeOfToll.DayOfWeek == DayOfWeek.Saturday) ||
                (timeOfToll.DayOfWeek == DayOfWeek.Sunday))
            {
                return 1.0m;
            }

            int hour = timeOfToll.Hour;
            if (hour < 6)
            {
                return 0.75m;
            }
            else if (hour < 10)
            {
                return inbound ? 2.0m : 1.0m;
            }
            else if (hour < 16)
            {
                return 1.5m;
            }
            else if (hour < 20)
            {
                return inbound ? 1.0m : 2.0m;
            }
            else
            {
                return 0.75m;
            }
        }
    }
}
