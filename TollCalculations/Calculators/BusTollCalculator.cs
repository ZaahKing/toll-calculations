﻿using System;
using LiveryRegistration;

namespace TollCalculations
{
    /// <summary>BusTollCalculator.</summary>
    public class BusTollCalculator : VehicleTollCalculator
    {
        private readonly Bus bus;

        /// <summary>Initializes a new instance of the <see cref="BusTollCalculator" /> class.</summary>
        /// <param name="bus">The bus.</param>
        /// <exception cref="System.ArgumentNullException">bus.</exception>
        public BusTollCalculator(Bus bus)
            => this.bus = bus ?? throw new ArgumentNullException(nameof(bus));

        /// <inheritdoc/>
        public override decimal CalculateToll()
        {
            var temp = (double)this.bus.Riders / (double)this.bus.Capacity;
            if (temp < 0.50)
            {
                return 5.00m + 2.00m;
            }

            if (temp > 0.90)
            {
                return 5.00m - 1.00m;
            }

            return 5.00m;
        }
    }
}
