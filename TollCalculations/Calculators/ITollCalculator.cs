﻿using System;

namespace TollCalculations
{
    /// <summary>ITollCalculator.</summary>
    public interface ITollCalculator
    {
        /// <summary>Calculates the toll.</summary>
        /// <returns>Toll amount.</returns>
        decimal CalculateToll();

        /// <summary>Peaks the time premium.</summary>
        /// <param name="timeOfToll">The time of toll.</param>
        /// <param name="inbound">if set to <c>true</c> [inbound].</param>
        /// <returns>Premium.</returns>
        decimal PeakTimePremium(DateTime timeOfToll, bool inbound);
    }
}
