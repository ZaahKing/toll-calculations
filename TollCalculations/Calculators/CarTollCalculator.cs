﻿using System;
using ConsumerVehicleRegistration;

namespace TollCalculations
{
    /// <summary>CarTollCalculator.</summary>
    public class CarTollCalculator : VehicleTollCalculator
    {
        private readonly Car car;

        /// <summary>Initializes a new instance of the <see cref="CarTollCalculator" /> class.</summary>
        /// <param name="car">The car.</param>
        /// <exception cref="System.ArgumentNullException">car.</exception>
        public CarTollCalculator(Car car) => this.car = car ?? throw new ArgumentNullException(nameof(car));

        /// <inheritdoc/>
        public override decimal CalculateToll()
        {
            return this.car.Passengers switch
            {
                0 => 2.00m + 0.5m,
                1 => 2.0m,
                2 => 2.0m - 0.5m,
                _ => 2.00m - 1.0m
            };
        }
    }
}
