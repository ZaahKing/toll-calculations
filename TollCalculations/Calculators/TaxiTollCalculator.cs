﻿using System;
using LiveryRegistration;

namespace TollCalculations
{
    /// <summary>TaxiTollCalculator.</summary>
    public class TaxiTollCalculator : VehicleTollCalculator
    {
        private readonly Taxi taxi;

        /// <summary>Initializes a new instance of the <see cref="TaxiTollCalculator" /> class.</summary>
        /// <param name="taxi">The taxi.</param>
        /// <exception cref="System.ArgumentNullException">taxi.</exception>
        public TaxiTollCalculator(Taxi taxi)
            => this.taxi = taxi ?? throw new ArgumentNullException(nameof(taxi));

        /// <inheritdoc/>
        public override decimal CalculateToll()
        {
            return this.taxi.Fares switch
            {
                0 => 3.50m + 1.00m,
                1 => 3.50m,
                2 => 3.50m - 0.50m,
                _ => 3.50m - 1.00m
            };
        }
    }
}
