﻿using System;
using CommercialRegistration;

namespace TollCalculations
{
    /// <summary>DeliveryTruckTollCalculator.</summary>
    public class DeliveryTruckTollCalculator : VehicleTollCalculator
    {
        private readonly DeliveryTruck truck;

        /// <summary>Initializes a new instance of the <see cref="DeliveryTruckTollCalculator" /> class.</summary>
        /// <param name="truck">The truck.</param>
        /// <exception cref="System.ArgumentNullException">truck.</exception>
        public DeliveryTruckTollCalculator(DeliveryTruck truck)
            => this.truck = truck ?? throw new ArgumentNullException(nameof(truck));

        /// <inheritdoc/>
        public override decimal CalculateToll()
        {
            if (this.truck.GrossWeightClass > 5000)
            {
                return 10.00m + 5.00m;
            }

            if (this.truck.GrossWeightClass < 3000)
            {
                return 10.00m - 2.00m;
            }

            return 10.00m;
        }
    }
}
