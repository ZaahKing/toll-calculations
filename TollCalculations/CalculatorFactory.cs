﻿using System;
using CommercialRegistration;
using ConsumerVehicleRegistration;
using LiveryRegistration;

namespace TollCalculations
{
    /// <summary>CalculatorFactory.</summary>
    public static class CalculatorFactory
    {
        /// <summary>Gets the calculator.</summary>
        /// <param name="obj">The object.</param>
        /// <returns>Toll calculator.</returns>
        /// <exception cref="System.ArgumentNullException">When get nothing.</exception>
        /// <exception cref="System.ArgumentException">If type has no known.</exception>
        public static ITollCalculator GetCalculator(object obj)
        {
            return obj switch
            {
                Car car => GetCalculator(car),
                Bus bus => GetCalculator(bus),
                Taxi taxi => GetCalculator(taxi),
                DeliveryTruck truck => GetCalculator(truck),
                _ => throw new ArgumentException(message: "Not a known vehicle type", paramName: nameof(obj)),
            };
        }

        private static ITollCalculator GetCalculator(Car car)
        {
            return new CarTollCalculator(car);
        }

        private static ITollCalculator GetCalculator(DeliveryTruck truck)
        {
            return new DeliveryTruckTollCalculator(truck);
        }

        private static ITollCalculator GetCalculator(Taxi taxi)
        {
            return new TaxiTollCalculator(taxi);
        }

        private static ITollCalculator GetCalculator(Bus bus)
        {
            return new BusTollCalculator(bus);
        }
    }
}
