﻿namespace ConsumerVehicleRegistration
{
    /// <summary>Car.</summary>
    public class Car
    {
        /// <summary>Gets or sets the passengers.</summary>
        /// <value>The passengers.</value>
        public int Passengers { get; set; }
    }
}
