﻿namespace CommercialRegistration
{
    /// <summary>DeliveryTruck.</summary>
    public class DeliveryTruck
    {
        /// <summary>Gets or sets the gross weight class.</summary>
        /// <value>The gross weight class.</value>
        public int GrossWeightClass { get; set; }
    }
}
