﻿namespace LiveryRegistration
{
    /// <summary>Taxi.</summary>
    public class Taxi
    {
        /// <summary>Gets or sets the fares.</summary>
        /// <value>The fares.</value>
        public int Fares { get; set; }
    }
}
