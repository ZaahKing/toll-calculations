﻿using LiveryRegistration;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TollCalculations.Test
{
    [TestFixture]
    public class BusTollCalculatorTest
    {
        private readonly ITollCalculator calculator = CalculatorFactory.GetCalculator(new Bus { Riders = 1, Capacity = 1 });

        public static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(new Bus { Riders = 0, Capacity = 1 }, 7m);
                yield return new TestCaseData(new Bus { Riders = 49, Capacity = 100 }, 7m);
                yield return new TestCaseData(new Bus { Riders = 5, Capacity = 10 }, 5m);
                yield return new TestCaseData(new Bus { Riders = 89, Capacity = 100 }, 5m);
                yield return new TestCaseData(new Bus { Riders = 1, Capacity = 1 }, 4m);
            }
        }

        [TestCaseSource(nameof(DataCases))]
        public void CalculateToll_Busses_CorrectToll(Bus bus, decimal expected)
            => Assert.AreEqual(expected, new BusTollCalculator(bus).CalculateToll());

       [TestCaseSource(typeof(PeakTimePremiumSource), nameof(DataCases))]
        public void PeakTimePremium_DateTimeAndInBound_CorrectCoefficient(DateTime dateTime, bool inBound, decimal expected)
        {
            decimal actual = this.calculator.PeakTimePremium(dateTime, inBound);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Constructor_Null_ArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => new BusTollCalculator(null));
    }
}
