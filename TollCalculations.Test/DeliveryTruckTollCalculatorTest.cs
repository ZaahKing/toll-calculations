﻿using System;
using System.Collections.Generic;
using CommercialRegistration;
using NUnit.Framework;

namespace TollCalculations.Test
{
    [TestFixture]
    public class DeliveryTruckTollCalculatorTest
    {
        private readonly ITollCalculator calculator = CalculatorFactory.GetCalculator(new DeliveryTruck { GrossWeightClass = 5000 });

        public static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 0 }, 8m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 2999 }, 8m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 3000 }, 10m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 5000 }, 10m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 5001 }, 15m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 7000 }, 15m);
            }
        }

        [TestCaseSource(nameof(DataCases))]
        public void CalculateToll_DeliveryTrucks_CorrectToll(DeliveryTruck truck, decimal expected)
            => Assert.AreEqual(expected, new DeliveryTruckTollCalculator(truck).CalculateToll());

        [TestCaseSource(typeof(PeakTimePremiumSource), nameof(DataCases))]
        public void PeakTimePremium_DateTimeAndInBound_CorrectCoefficient(DateTime dateTime, bool inBound, decimal expected)
        {
            decimal actual = this.calculator.PeakTimePremium(dateTime, inBound);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Constructor_Null_ArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => new DeliveryTruckTollCalculator(null));
    }
}