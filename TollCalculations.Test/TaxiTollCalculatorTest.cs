﻿using LiveryRegistration;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TollCalculations.Test
{
    [TestFixture]
    public class TaxiTollCalculatorTest
    {
        private readonly ITollCalculator calculator = CalculatorFactory.GetCalculator(new Taxi { Fares = 1 });

        public static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(new Taxi { Fares = 0 }, 4.5m);
                yield return new TestCaseData(new Taxi { Fares = 1 }, 3.5m);
                yield return new TestCaseData(new Taxi { Fares = 2 }, 3.0m);
                yield return new TestCaseData(new Taxi { Fares = 3 }, 2.5m);
            }
        }

        [TestCaseSource(nameof(DataCases))]
        public void CalculateToll_Taxies_CorrectToll(Taxi taxi, decimal expected)
            => Assert.AreEqual(expected, new TaxiTollCalculator(taxi).CalculateToll());

        [TestCaseSource(typeof(PeakTimePremiumSource), nameof(DataCases))]
        public void PeakTimePremium_DateTimeAndInBound_CorrectCoefficient(DateTime dateTime, bool inBound, decimal expected)
        {
            decimal actual = this.calculator.PeakTimePremium(dateTime, inBound);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Constructor_Null_ArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => new TaxiTollCalculator(null));
    }
}
