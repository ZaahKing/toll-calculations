﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace TollCalculations.Test
{
    public static class PeakTimePremiumSource
    {
        public static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(new DateTime(2021, 5, 29,  0, 0, 0), true, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29,  10, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29,  19, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29, 6, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29, 0, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29, 10, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29, 19, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 29, 6, 0, 0), false, 1.0m);

                yield return new TestCaseData(new DateTime(2021, 5, 28, 6, 0, 0), true, 2.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 6, 0, 0), false, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 9, 59, 59), true, 2.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 9, 59, 59), false, 1.0m);

                yield return new TestCaseData(new DateTime(2021, 5, 28, 10, 0, 0), true, 1.5m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 10, 0, 0), false, 1.5m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 15, 59, 59), true, 1.5m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 15, 59, 59), false, 1.5m);

                yield return new TestCaseData(new DateTime(2021, 5, 28, 16, 0, 0), true, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 16, 0, 0), false, 2.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 19, 59, 59), true, 1.0m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 19, 59, 59), false, 2.0m);

                yield return new TestCaseData(new DateTime(2021, 5, 28, 20, 0, 0), true, 0.75m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 20, 0, 0), false, 0.75m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 5, 59, 59), false, 0.75m);
                yield return new TestCaseData(new DateTime(2021, 5, 28, 5, 59, 59), true, 0.75m);
            }
        }
    }
}
