﻿using System;
using System.Collections.Generic;
using ConsumerVehicleRegistration;
using NUnit.Framework;

namespace TollCalculations.Test
{
    [TestFixture]
    public class CarTollCalculatorTest
    {
        private readonly ITollCalculator calculator = CalculatorFactory.GetCalculator(new Car { Passengers = 2 });

        public static IEnumerable<TestCaseData> DataCases
        {
            get
            {
                yield return new TestCaseData(new Car { Passengers = 0 }, 2.5m);
                yield return new TestCaseData(new Car { Passengers = 1 }, 2.0m);
                yield return new TestCaseData(new Car { Passengers = 2 }, 1.5m);
                yield return new TestCaseData(new Car { Passengers = 3 }, 1.0m);
            }
        }

        [TestCaseSource(nameof(DataCases))]
        public void CalculateToll_CarWithDifferentsPasangersAmount_CorrectToll(Car car, decimal expected)
            => Assert.AreEqual(expected, new CarTollCalculator(car).CalculateToll());

        [TestCaseSource(typeof(PeakTimePremiumSource), nameof(DataCases))]
        public void PeakTimePremium_DateTimeAndInBound_CorrectCoefficient(DateTime dateTime, bool inBound, decimal expected)
        {
            decimal actual = this.calculator.PeakTimePremium(dateTime, inBound);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void Constructor_Null_ArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => new CarTollCalculator(null));
    }
}
