using CommercialRegistration;
using ConsumerVehicleRegistration;
using LiveryRegistration;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace TollCalculations.Test
{
    [TestFixture]
    public class CalculatorFactoryTests
    {
        public static IEnumerable<TestCaseData> TollCalculatorDataCases
        {
            get
            {
                yield return new TestCaseData(new Car { Passengers = 0 }, 2.5m);
                yield return new TestCaseData(new Car { Passengers = 1 }, 2.0m);
                yield return new TestCaseData(new Car { Passengers = 2 }, 1.5m);
                yield return new TestCaseData(new Car { Passengers = 3 }, 1.0m);
                yield return new TestCaseData(new Taxi { Fares = 0 }, 4.5m);
                yield return new TestCaseData(new Taxi { Fares = 1 }, 3.5m);
                yield return new TestCaseData(new Taxi { Fares = 2 }, 3.0m);
                yield return new TestCaseData(new Taxi { Fares = 3 }, 2.5m);
                yield return new TestCaseData(new Bus { Riders = 0, Capacity = 1 }, 7m);
                yield return new TestCaseData(new Bus { Riders = 49, Capacity = 100 }, 7m);
                yield return new TestCaseData(new Bus { Riders = 5, Capacity = 10 }, 5m);
                yield return new TestCaseData(new Bus { Riders = 89, Capacity = 100 }, 5m);
                yield return new TestCaseData(new Bus { Riders = 1, Capacity = 1 }, 4m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 0 }, 8m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 2999 }, 8m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 3000 }, 10m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 5000 }, 10m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 5001 }, 15m);
                yield return new TestCaseData(new DeliveryTruck { GrossWeightClass = 7000 }, 15m);
            }
        }

        [TestCaseSource(nameof(TollCalculatorDataCases))]
        public void GetCalculator_Vehicle_CorrectToll(object vehicle, decimal expected)
            => Assert.AreEqual(expected, CalculatorFactory.GetCalculator(vehicle).CalculateToll());

        [Test]
        public void GetCalculator_Null_ArgumentNullExeption()
            => Assert.Throws<ArgumentNullException>(() => CalculatorFactory.GetCalculator((object)null));

        [TestCase(1)]
        [TestCase("Car")]
        public void GetCalculator_NonVehicle_ArgumentExeption(object obj)
            => Assert.Throws<ArgumentException>(() => CalculatorFactory.GetCalculator(obj));
    }
}